package danit.homeworktasks.javaspring.utilities;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT )
public enum Currency {
    USD("USD"),
    EUR("EUR"),
    UAH("UAH"),
    CHF("CHF"),
    GBP("GBP");

    private String val;

    Currency(String val) {
        this.val = val;
    }

 /*   @JsonValue*/
    public String getValue() {
        return val;
    }
}
