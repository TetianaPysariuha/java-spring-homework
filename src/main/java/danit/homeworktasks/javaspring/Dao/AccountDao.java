package danit.homeworktasks.javaspring.Dao;

import danit.homeworktasks.javaspring.model.Account;
import danit.homeworktasks.javaspring.utilities.Currency;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class AccountDao implements Dao<Account> {
    private List<Account> accounts;
    @Autowired
    private CustomerDao customerDao;

    private Long idCounter = 0L;

   @PostConstruct
   public void init() {
       accounts = new ArrayList<>();
       this.save(new Account(customerDao.getOne(1L), Currency.UAH));
       this.save(new Account(customerDao.getOne(1L), Currency.USD));
       this.save(new Account(customerDao.getOne(2L), Currency.UAH));
       this.save(new Account(customerDao.getOne(2L), Currency.EUR));
       this.save(new Account(customerDao.getOne(3L), Currency.EUR));
       this.save(new Account(customerDao.getOne(3L), Currency.CHF));
       this.save(new Account(customerDao.getOne(3L), Currency.USD));
       this.save(new Account(customerDao.getOne(3L), Currency.GBP));
       this.save(new Account(customerDao.getOne(3L), Currency.UAH));
       this.save(new Account(customerDao.getOne(4L), Currency.UAH));
       customerDao.findAll().forEach(user -> user.setAccounts(this.findAll().stream().filter(el -> el.getCustomer().equals(user)).toList()));
   }

    @Override
    public Account save(Account newAccount) {
        if(!accounts.contains(newAccount)){
            newAccount.setId(++idCounter);
            accounts.add(newAccount);
        }
        return newAccount;
    }

    @Override
    public boolean delete(Account delAccount) {
        return accounts.remove(delAccount);
    }

    @Override
    public void deleteAll(List<Account> entities) {
        accounts.removeAll(entities);
    }

    @Override
    public void saveAll(List<Account> entities) {
        accounts.addAll(entities);
    }

    @Override
    public List<Account> findAll() {
        return accounts;
    }

    @Override
    public boolean deleteById(long id) {
        List<Account> delOneAccountList = accounts.stream().filter(el -> el.getId().equals(id)).toList();
        return delOneAccountList.size() > 0 && accounts.remove(delOneAccountList.get(0));
    }

    @Override
    public Account getOne(long id) {
        List<Account> getOneAccountList = accounts.stream().filter(el -> el.getId().equals(id)).toList();
        return getOneAccountList.size() > 0 ? getOneAccountList.get(0) : null;
    }

    public Account findByNumber(String number){
        List<Account> filteredAccounts = accounts.stream().filter(el -> el.getNumber().equals(number)).toList();
        return filteredAccounts.size() > 0 ? filteredAccounts.get(0) : null;
    }
}
