package danit.homeworktasks.javaspring.Dao;

import danit.homeworktasks.javaspring.model.Customer;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerDao implements Dao<Customer> {
    List<Customer> customers;

    private Long idCounter = 0L;
/*    public Long getIdCounter() {
        return ++idCounter;
    }*/

    @PostConstruct
    public void init() {
        customers = new ArrayList<>();
        this.save(new Customer("Alise Brown", "AliseBrown@gmail.com", 25));
        this.save(new Customer("Adam Smith", "AdamSmith@gmail.com", 36));
        this.save(new Customer("Jaime Yellow", "JaimeYellow@gmail.com", 55));
        this.save(new Customer("Antony Blue", "AntonyBlue@gmail.com", 44));
    }
    @Override
    public Customer save(Customer newCustomer) {
        Customer existCustomerById = null;

        if(newCustomer.getId() != null){
            existCustomerById = this.getOne(newCustomer.getId());
            System.out.println("existCustomerById = " + existCustomerById);
        }
        if(!customers.contains(newCustomer) && existCustomerById == null){
            newCustomer.setId(++idCounter);
            customers.add(newCustomer);
        } else if (existCustomerById != null){
            customers.remove(existCustomerById);
            System.out.println("customers = " + customers);
            customers.add(newCustomer);
            System.out.println("newCustomer = " + newCustomer);
        }
        return this.getOne(newCustomer.getId());
    }

    @Override
    public boolean delete(Customer delCustomer) {
            return customers.remove(delCustomer);
    }

    @Override
    public void deleteAll(List<Customer> entities) {
        customers.removeAll(entities);
    }

    @Override
    public void saveAll(List<Customer> entities) {
        customers.addAll(entities);
    }

    @Override
    public List<Customer> findAll() {
        return customers;
    }

    @Override
    public boolean deleteById(long id) {
        List<Customer> delOneCustomerList = customers.stream().filter(el -> el.getId().equals(id)).toList();
        return delOneCustomerList.size() > 0 ? customers.remove(delOneCustomerList.get(0)) : false;
    }

    @Override
    public Customer getOne(long id) {
        List<Customer> getOneCustomerList = customers.stream().filter(el -> el.getId().equals(id)).toList();
        return getOneCustomerList.size() > 0 ? getOneCustomerList.get(0) : null;
    }

}
