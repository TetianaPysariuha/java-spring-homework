package danit.homeworktasks.javaspring.controller;

import danit.homeworktasks.javaspring.utilities.Currency;
import danit.homeworktasks.javaspring.service.CustomerService;
import danit.homeworktasks.javaspring.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"http://localhost:3000"})
@RequestMapping("/customers")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping("/")
    public ResponseEntity<?> getAllUsers(){
        return ResponseEntity.ok(customerService.getAllCustomers());
    }
    @GetMapping("/{id}")
    public Customer getUserFullInfo(@PathVariable Long id){
        return customerService.getCustomerFullInfo(id);
    }
    @PostMapping("/")
    public Customer postNewUser(@RequestBody Customer customer){
        return customerService.createCustomer(customer);
    }
    @PutMapping("/")
    public Customer updateUser(@RequestBody Customer customer){
        return customerService.updateCustomer(customer);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Long id){
        return customerService.deleteCustomerById(id) ? ResponseEntity.ok("Success") : ResponseEntity.badRequest().body("Customer not found");
    }
    @PostMapping("/{id}/account")
    public ResponseEntity<?> openAccount(@PathVariable Long id, @RequestBody Currency currency){
         Customer customer = customerService.getCustomerFullInfo(id);
         if(customer == null) {
             ResponseEntity.badRequest().body("Customer not found");
         }
        return ResponseEntity.ok(customerService.openAccount(customer, currency));
    }
    @DeleteMapping("/{id}/account")
    public ResponseEntity<?> openAccount(@PathVariable Long id, @RequestBody String accountNumber ){
        Customer customer = customerService.getCustomerFullInfo(id);
        if(customer == null) {
            ResponseEntity.badRequest().body("Customer not found");
        }
        return ResponseEntity.ok(customerService.deleteAccount(customer, accountNumber));
    }
}
