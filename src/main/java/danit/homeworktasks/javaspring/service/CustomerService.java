package danit.homeworktasks.javaspring.service;

import danit.homeworktasks.javaspring.Dao.AccountDao;
import danit.homeworktasks.javaspring.Dao.CustomerDao;
import danit.homeworktasks.javaspring.utilities.Currency;
import danit.homeworktasks.javaspring.model.Account;
import danit.homeworktasks.javaspring.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    private CustomerDao customerDao;
    private AccountDao accountDao;

    public CustomerService(){}
    @Autowired
    public CustomerService(CustomerDao customerDao, AccountDao accountDao) {
        this.customerDao = customerDao;
        this.accountDao = accountDao;
    }

    public Customer getCustomerFullInfo(Long id){
        return customerDao.getOne(id);
    }

    public List<Customer> getAllCustomers(){
        return customerDao.findAll();
    }

    public Customer createCustomer(Customer newCustomer){
        return customerDao.save(newCustomer);
    }
    public Customer createCustomer(String name, String email, Integer age){
        System.out.println(name + "/" + email + "/" + age);
        Customer newCustomer = new Customer(name, email, age);
        return customerDao.save(newCustomer);
    }

    public Customer updateCustomer(Customer customer){
        if(customer.getId() == null){
            return null;
        }
        customerDao.save(customer);
        return customer;
    }

    public Customer updateCustomer(Long id, String name, String email, Integer age){
        if(id == null){
            return null;
        }
        Customer customer = new Customer(name, email, age);
        customer.setId(id);
        customerDao.save(customer);
        return customer;
    }

    public boolean deleteCustomer(Customer customer){
        accountDao.deleteAll(customer.getAccounts());
        return customerDao.delete(customer);
    }
    public boolean deleteCustomerById(Long id){
        Customer currentCustomer = this.getCustomerFullInfo(id);
        return this.deleteCustomer(currentCustomer);
    }
    public Customer openAccount(Customer customer, Currency currency){
        Account newAccount = new Account(customer, currency);
        accountDao.save(newAccount);
        customer.getAccounts().add(newAccount);
        return customer;
    }
    public Customer deleteAccount(Customer customer, String number){
        Account account = accountDao.findByNumber(number);
        if(account.getCustomer().equals(customer)){
            accountDao.delete(account);
            customer.getAccounts().remove(account);
        }
        return customer;
    }
}
